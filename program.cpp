#include <iostream>
#include <string>
#include <sstream>
#include <vector>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <GL/glextl.h>
#include "glfw-setup.hpp"
#include "shader.h"

using namespace std;

#define AddAttribute(t, m, at) Add(Attribute(offsetof(t, m), #m, at))
#define AddUniform(t, m, at) Add(Uniform(offsetof(t, m), #m, at))
#define AddVarying(t, m, at) Add(Varying(offsetof(t, m), #m, at))

namespace glslTypes
{
    enum eGlslTypes
    {
        vec2,
        vec3,
        vec4,
        mat4,
        sampler2D
    };

    string toString(eGlslTypes t)
    {
        switch (t)
        {
        case vec2: return "vec2";
        case vec3: return "vec3";
        case vec4: return "vec4";
        case mat4: return "mat4";
        case sampler2D: return "sampler2D";
        }
    }
}

class Attribute
{
public:
    Attribute(size_t o, const char* n, glslTypes::eGlslTypes t) : offset(o), name(n), type(t) { }

    size_t offset;
    const char* name;
    glslTypes::eGlslTypes type;
};

class Uniform
{
public:
    Uniform(size_t o, const char* n, glslTypes::eGlslTypes t) : offset(o), name(n), type(t) { }

    size_t offset;
    const char* name;
    glslTypes::eGlslTypes type;
};

class Varying
{
public:
    Varying(size_t o, const char* n, glslTypes::eGlslTypes t) : offset(o), name(n), type(t) { }

    size_t offset;
    const char* name;
    glslTypes::eGlslTypes type;
};

template<class VertexType, class UniformType>
class BaseShader
{
public:
    unsigned int _program, _vao, _vbo;
    int _bufferSize;

    vector<Uniform> _uniforms;

    void compile(const vector<Attribute>& attributes, const string& vertShader, const string& fragShader)
    {
        glGenVertexArrays(1, &this->_vao);
        glBindVertexArray(this->_vao);
        glGenBuffers(1, &this->_vbo);
        glBindBuffer(GL_ARRAY_BUFFER, this->_vbo);

        this->_program = LoadShaderProgram(vertShader, fragShader);

        for (auto itr = attributes.begin(); itr != attributes.end(); ++itr)
        {
            Attribute attr = *itr;
            GLuint loc = static_cast<GLuint>(glGetAttribLocation(this->_program, attr.name));
            glVertexAttribPointer(loc, 3, GL_FLOAT, GL_FALSE, sizeof(VertexType), reinterpret_cast<const void*>(attr.offset));
            glEnableVertexAttribArray(loc);
        }

        glBindVertexArray(0);

        glUseProgram(this->_program);
    }

    void use()
    {
        glUseProgram(this->_program);
    }

    void setUniforms(UniformType& uniformValues)
    {
        char* shaderBuffer = reinterpret_cast<char*>(&uniformValues);
        for (auto itr = this->_uniforms.begin(); itr != this->_uniforms.end(); ++itr)
        {
            Uniform uniform = *itr;
            auto loc = glGetUniformLocation(this->_program, uniform.name);
            if (uniform.type == glslTypes::mat4)
            {
                glm::mat4& m = *reinterpret_cast<glm::mat4*>(shaderBuffer+uniform.offset);
                glUniformMatrix4fv(loc, 1, false, glm::value_ptr(m));
            }
        }
    }

    void setBuffer(VertexType* vertices, int count)
    {
        int size = count * static_cast<int>(sizeof(VertexType));
        this->_bufferSize = count;
        glBindVertexArray(this->_vao);
        glBufferData(GL_ARRAY_BUFFER, size, 0, GL_STATIC_DRAW);
        glBufferSubData(GL_ARRAY_BUFFER, 0, size, reinterpret_cast<GLvoid*>(vertices));
    }

    void render(GLenum mode)
    {
        glDrawArrays(mode, 0, this->_bufferSize);
    }
};

template<class VertexType, class ShaderType>
class ShaderBuilder
{
    vector<Attribute> _attributes;
    vector<Uniform> _vertexUniforms;
    vector<Varying> _varyings;
    vector<Uniform> _fragmentUniforms;
    std::string _vertMain;
    std::string _fragMain;
    int _version;

public:
    ShaderBuilder(int version = 0)
        : _vertMain(""), _fragMain(""), _version(version)
    { }

    ShaderBuilder& Add(const Attribute& attr)
    {
        this->_attributes.push_back(attr);
        return *this;
    }

    ShaderBuilder& Add(const Varying& var)
    {
        this->_varyings.push_back(var);
        return *this;
    }

    ShaderBuilder& Add(const Uniform& uniform)
    {
        if (this->_vertMain == "")
            this->_vertexUniforms.push_back(uniform);
        else
            this->_fragmentUniforms.push_back(uniform);

        return *this;
    }

    ShaderBuilder& VertMain(const char* code)
    {
        this->_vertMain = code;

        return *this;
    }

    ShaderBuilder& FragMain(const char* code)
    {
        this->_fragMain = code;

        return *this;
    }

    BaseShader<VertexType, ShaderType>* Setup()
    {
        auto shader = new BaseShader<VertexType, ShaderType>();

        // Add all uniforms to the base shader
        shader->_uniforms.insert(shader->_uniforms.end(), this->_vertexUniforms.begin(), this->_vertexUniforms.end());
        shader->_uniforms.insert(shader->_uniforms.end(), this->_fragmentUniforms.begin(), this->_fragmentUniforms.end());

        // compile the base shader and setup the buffers from the attributes
        shader->compile(this->_attributes, this->WriteVertexShader(), this->WriteFragmentShader());

        return shader;
    }

private:
    string WriteVertexShader()
    {
        stringstream ss;

        if (this->_version > 0)
            ss << "#version " << this->_version << endl << endl;

        for (auto itr = this->_attributes.begin(); itr != this->_attributes.end(); ++itr)
            ss << "in " << glslTypes::toString(itr->type) << " " << itr->name << ";" << endl;

        for (auto itr = this->_vertexUniforms.begin(); itr != this->_vertexUniforms.end(); ++itr)
            ss << "uniform " << glslTypes::toString(itr->type) << " " << itr->name << ";" << endl;

        for (auto itr = this->_varyings.begin(); itr != this->_varyings.end(); ++itr)
            ss << "out " << glslTypes::toString(itr->type) << " " << itr->name << ";" << endl;

        ss << endl
           << "void main() {" << endl
           << this->_vertMain << endl
           << "}" << endl;

        return ss.str();
    }

    string WriteFragmentShader()
    {
        stringstream ss;

        if (this->_version > 0)
            ss << "#version " << this->_version << endl << endl;

        for (auto itr = this->_fragmentUniforms.begin(); itr != this->_fragmentUniforms.end(); ++itr)
            ss << "uniform " << glslTypes::toString(itr->type) << " " << itr->name << ";" << endl;

        for (auto itr = this->_varyings.begin(); itr != this->_varyings.end(); ++itr)
            ss << "in " << glslTypes::toString(itr->type) << " " << itr->name << ";" << endl;

        ss << endl
           << "void main() {" << endl
           << this->_fragMain << endl
           << "}" << endl;

        return ss.str();
    }
};


/******************************************************************************************/
/******************************************************************************************/
/******************************************************************************************/


struct Vertex
{
    glm::vec3 position;
    glm::vec3 normal;
    glm::vec3 texcoord;
};

struct Shader
{
    // uniforms
    glm::mat4 u_projection;
    glm::mat4 u_view;
    glm::mat4 u_model;

    // fragment uniforms
    unsigned int tex;

};

class Program : public GlfwProgram
{
public:
    Program();

    BaseShader<Vertex, Shader>* shader;
    virtual bool SetUp();
    virtual void Render();
    virtual void CleanUp();

    virtual void OnKeyAction(int /*key*/, int /*scancode*/, int /*action*/, int /*mods*/) { }
    virtual void OnResize(int /*width*/, int /*height*/);

};

Program::Program() : shader(nullptr) { }

bool Program::SetUp()
{
    this->shader = ShaderBuilder<Vertex, Shader>(150)
            .AddAttribute(Vertex, position, glslTypes::vec3)
            .AddAttribute(Vertex, normal, glslTypes::vec3)
            .AddAttribute(Vertex, texcoord, glslTypes::vec3)
            .AddUniform(Shader, u_projection, glslTypes::mat4)
            .AddUniform(Shader, u_view, glslTypes::mat4)
            .AddUniform(Shader, u_model, glslTypes::mat4)
            .VertMain("gl_Position = u_projection * u_view * u_model * vec4(position.xyz, 1.0);")
            .AddUniform(Shader, tex, glslTypes::sampler2D)
            .FragMain("gl_FragColor = vec4(0.0, 1.0, 0.0, 1.0);")
            .Setup();

    Vertex v[4] = {
        { glm::vec3(-0.5f, 0.5f, 0.0f), glm::vec3(), glm::vec3() },
        { glm::vec3(0.5f, 0.5f, 0.0f), glm::vec3(), glm::vec3() },
        { glm::vec3(0.5f, -0.5f, 0.0f), glm::vec3(), glm::vec3() },
        { glm::vec3(-0.5f, -0.5f, 0.0f), glm::vec3(), glm::vec3() }
    };
    this->shader->setBuffer(v, 4);

    return true;
}

void Program::OnResize(int width, int height)
{
    glViewport(0, 0, width, height);

    Shader s = {
        /* projection */ glm::ortho(-float(width), float(width), -float(height), float(height), -10.0f, 10.0f),
        /* view       */ glm::translate(glm::mat4(1.0f), glm::vec3(0.5f, 0.5f, 0.0f)),
        /* model      */ glm::scale(glm::mat4(1.0f), glm::vec3(100.0f, 100.0f, 100.0f)),
        /* tex        */ 0
    };

    this->shader->setUniforms(s);
}

void Program::Render()
{
    glm::mat4 m;
    this->shader->use();

    this->shader->render(GL_LINE_LOOP);
}

void Program::CleanUp()
{
    if (shader != nullptr) delete shader;
    shader = nullptr;
}

int main(int argc, char* argv[])
{
    return Program().Run(argc, argv);
}
