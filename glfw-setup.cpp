
#define GLEXTL_IMPLEMENTATION
#include <GL/glextl.h>

#include "glfw-setup.hpp"
#include <GLFW/glfw3.h>

GlfwProgram* GlfwProgram::program = nullptr;

GlfwProgram::GlfwProgram()
    : width(640), height(480), title("GlfwWindow")
{
    GlfwProgram::program = this;
}

GlfwProgram::~GlfwProgram()
{ }

int GlfwProgram::Run(int argc, char* argv[])
{
    for (int i = 1; i < argc; i++)
    {
        this->args.push_back(argv[i]);
    }

    if (glfwInit() == GLFW_FALSE)
        return -1;

    this->window = glfwCreateWindow(this->width, this->height, this->title.c_str(), NULL, NULL);
    if (this->window == nullptr)
    {
        glfwTerminate();
        return -1;
    }

    glfwSetKeyCallback(this->window, GlfwProgram::KeyActionCallback);
    glfwSetFramebufferSizeCallback(this->window, GlfwProgram::ResizeCallback);

    glfwMakeContextCurrent(this->window);

    glExtLoadAll((PFNGLGETPROC*)glfwGetProcAddress);

    if (this->SetUp())
    {
        GlfwProgram::ResizeCallback(this->window, this->width, this->height);

        while (glfwWindowShouldClose(this->window) == 0)
        {
            glfwPollEvents();

            glClear(GL_COLOR_BUFFER_BIT);

            this->Render();

            glfwSwapBuffers(this->window);

        }
        this->CleanUp();
    }

    glfwTerminate();

    return 0;
}
